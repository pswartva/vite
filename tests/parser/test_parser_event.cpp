/**
 *
 * @file tests/parser/test_parser_event.cpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Johnny Jazeix
 * @author Mathieu Faverge
 * @author Pascal Noisette
 * @author Olivier Lagrasse
 *
 * @date 2024-07-17
 */
#include "../../src/parser/Line.hpp"
#include "../../src/parser/ParserDefinitionDecoder.hpp"
#include "../../src/parser/ParserEventDecoder.hpp"
#include "../stubs/Trace.hpp"
#include <iostream>
#include <fstream>
#include <cstdlib>

#define DIE_IF(condition,message) if (condition){       \
        cout << message << endl;                        \
        return 0;}


using namespace std;
void stop(){}
int main(int argc, char** argv){

    ParserDefinitionDecoder *parserdefinition = new ParserDefinitionDecoder();
    ParserEventDecoder      *parserevent = new ParserEventDecoder();
    Trace trace;
  
    Line line;
    if (argc<2)
        line.open("trace_to_parse.trace");
    else
        line.open(argv[1]);

    int linecount = 0;
    static const string percent = "%";
    string event_identity_string;
    int event_identity;
    
    while(!line.is_eof()){

	line.newline();

	if(line.starts_with(percent)){
	    parserdefinition->store_definition(line);
            linecount ++;
	}
	else if (!line.item(0, event_identity_string)){
            continue;
        }
	else{
            stop();
            
	    DIE_IF(sscanf(event_identity_string.c_str(), "%d", &event_identity) != 1, "expected identifier for a definition");

	    parserevent->store_event(
                                     parserdefinition->get_definition(event_identity),
                                     line,
                                     trace);

            ;
	}
    }

    //parserdefinition->print_definitions();
    cout << "lu :" << linecount << endl;

    delete parserdefinition;
    delete parserevent;
    return EXIT_SUCCESS;
}
