/**
 *
 * @file tests/parser/test_token.cpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Mathieu Faverge
 *
 * @date 2024-07-17
 */
#include <iostream>
#include "../../src/parser/TokenSource.hpp"
#include <fstream>


using namespace std;
//test de lexer
int main(int argc, char** argv)
{

    TokenSource source;
    if (argc<2)
        source.open("trace_to_parse.trace");
    else
        source.open(argv[1]);

    std::string* token;
    int i=0;
    while ((token=source.read_token())!=NULL)
    {
        i++;
        std::cout<<"-"<<*token<<"-"<<endl;
    }
    std::cout<<"token lu :"<<i<<"_"<<endl;
    return 0;
}
