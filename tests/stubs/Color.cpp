/**
 *
 * @file tests/stubs/Color.cpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Mathieu Faverge
 *
 * @date 2024-07-17
 */
#include "Color.hpp"

Color::Color(){
    me.erase();
}
std::string Color::to_string() const{
    return me;
}

bool Color::instantiate(const std::string &in, Color &out) {
    std::cout << "instantiate : " << in << " in color" << std::endl;
    return true;
}
