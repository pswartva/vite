/**
 *
 * @file tests/stubs/Integer.hpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Mathieu Faverge
 *
 * @date 2024-07-17
 */
#ifndef INTEGER_HPP
#define INTEGER_HPP

#include <iostream>
#include "Value.hpp"

/*!
 *
 * \file Integer.hpp
 * \author  NOISETTE
 * \brief Bouchon
 *
 */

class Integer : public Value{
private:
    std::string me;

public:
    Integer();
    std::string to_string() const;
    /*!
     *
     * \fn instantiate(const std::string &in, Double &out)
     * \brief Convert a string to a Double
     * \param in String to convert
     * \param out Double to be initialized
     * \return true, if the conversion succeeded
     *
     */
    static bool instantiate(const std::string &in, Integer &out);
};


#endif // INTEGER_HPP
