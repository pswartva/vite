/**
 *
 * @file plugins/MatrixVisualizer/Common/Zooming.hpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Arthur Chevalier
 * @author Johnny Jazeix
 * @author Mathieu Faverge
 * @author Hamza Benmendil
 *
 * @date 2024-07-17
 */
#ifndef ZOOMING_HPP
#define ZOOMING_HPP

#include "Zoom.hpp"

/**
 * \class Zooming
 * \brief This class contains one of the zoom methods, it is inherited from Zoom abstract class
 */
class Zooming : public Zoom
{
public:
    Zooming(symbol_matrix_t *matrix);
    ~Zooming();

    /**
     * \fn void move(double x_start, double x_end, double y_start, double y_end)
     * \brief The function that is called when the zoom is applied, it containts the implementation of the zoom method.
     * \param x_start The start of x coordinate of the selected zone
     * \param x_end The end of x coordinate of the selected zone
     * \param y_start The start of y coordinate of the selected zone
     * \param y_end The end of y coordinate of the selected zone
     */
    void move(double x_start, double x_end, double y_start, double y_end);
};

#endif
