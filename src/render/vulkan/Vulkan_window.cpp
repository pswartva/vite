/**
 *
 * @file src/render/vulkan/Vulkan_window.cpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Augustin Gauchet
 * @author Camille Ordronneau
 * @author Nolan Bredel
 * @author Lucas Guedon
 *
 * @date 2024-07-17
 */
#include "Vulkan_window.hpp"
#include <QVulkanInstance>
#include "Vulkan_window_renderer.hpp"

Vulkan_window::Vulkan_window(Interface_graphic *interface_graphic, Render_vulkan *main_renderer) :
    _interface_graphic(interface_graphic), _main_renderer(main_renderer) {
    _inst.setLayers(QByteArrayList() << "VK_LAYER_LUNARG_standard_validation");

    if (!_inst.create())
        qFatal("Failed to create Vulkan instance: %d", _inst.errorCode());

    setVulkanInstance(&_inst);
    resize(500, 500);
}

QVulkanWindowRenderer *Vulkan_window::createRenderer() {
    Vulkan_window_renderer *wr = new Vulkan_window_renderer(_interface_graphic, this);
    _main_renderer->set_window_renderer(wr);
    return wr;
}
