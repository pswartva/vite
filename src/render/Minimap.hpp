/**
 *
 * @file src/render/Minimap.hpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Johnny Jazeix
 * @author Augustin Degomme
 * @author Mathieu Faverge
 *
 * @date 2024-07-17
 */
/*!
 *\file Minimap.hpp
 */

#ifndef MINIMAP_HPP
#define MINIMAP_HPP

class QLabel;
class QImage;
class QPainter;
class QPen;
class QCloseEvent;

/*!
 * \brief This class redefined the QLabel widget to display a minimap.
 */
class Minimap : public QLabel
{
protected:
    /*!
     * \brief Viewport square coordinates and dimensions.
     */
    int _x, _y, _w, _h;

    /*!
     * \brief QImage to store the trace thumbnail.
     */
    QImage _minimap_image;

    /*!
     * \brief Store the original image before being scaled for _minimap_image.
     */
    QImage _original_image;

    /*!
     * \brief Painter used to draw the minimap image and current view square.
     */
    QPainter _painter;

    /*!
     * \brief Pen used with the painter.
     */
    QPen _pen;

    /*!
     * \brief Boolean used to know if the minimap needs to be initialized.
     */
    bool _is_initialized;

    /*!
     * \brief Called by Qt when the user manually closes the window.
     * \param event The Qt event.
     */
    void closeEvent(QCloseEvent *event) override;

    /*!
     * \brief Called by Qt when the user manually resize the window.
     * \param event The Qt event.
     */
    void resizeEvent(QResizeEvent *event) override;

    /*!
     * \brief Redraw the minimap according to the _minimap_image, _x, _y, _w and _h values.
     */
    void redraw();

public:
    /***********************************
     *
     * Constructor and destructor.
     *
     **********************************/

    /*!
     * \brief The default constructor
     */
    Minimap();

    /*!
     * \brief Init the minimap image.
     * \param image QImage used for the minimap image.
     */
    void init(const QImage &image);

    /*!
     * \brief Check if the minimap has been initialized
     */
    bool is_initialized();

    /*!
     * \brief Update the minimap according to the current viewport (viewport is define by x, y, w and h).
     */
    void update(const int x, const int y, const int w, const int h);

    /*!
     * \brief Release the minimap data.
     */
    void release();
};

#endif
