/**
 *
 * @file src/render/Render_svg.hpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Johnny Jazeix
 * @author Mathieu Faverge
 * @author Thibault Soucarre
 * @author Olivier Lagrasse
 * @author Augustin Degomme
 *
 * @date 2024-07-17
 */
/*!
 *\file Render_svg.hpp
 */

#ifndef RENDER_SVG_HPP
#define RENDER_SVG_HPP

/*!
 * \brief This class redefined the OpenGL widget - QGLWidget - to display the trace.
 */
class Render_svg : /*public QGLWidget,*/ public Render, public Geometry
{
protected:
    /*!
     * \brief buffer containing the main objets to be displayed
     */
    std::ostringstream _buffer;

    /*!
     * \brief output svg file
     */
    std::ofstream _output_file;

    float _red, _green, _blue;

public:
    /***********************************
     *
     * Constructor and destructor.
     *
     **********************************/

    /*!
     * \brief The default constructor
     */
    Render_svg(const std::string &output_filename);

    /*!
     * \brief The destructor
     */
    ~Render_svg() override;

    /*!
     * \brief Proceeds with the initialization of the OpenGL draw functions.
     */
    void start_draw() override;

    /*!
     * \brief Creates and opens the display list for container draws.
     */
    void start_draw_containers() override;

    /*!
     * \brief Draw a container according to the parameters
     * \param x the x position of the container
     * \param y the y position of the container
     * \param w the width of the container
     * \param h the height of the container
     */
    void draw_container(const Element_pos x, const Element_pos y, const Element_pos w, const Element_pos h);

    /*!
     * \brief Draw the text of a container.
     * \param x the x position of the text.
     * \param y the y position of the text.
     * \param value the string value of the text.
     *
     * This function stores text in a list. This list will be display each time the render area need to be updated.
     */
    void draw_container_text(const Element_pos x, const Element_pos y, const std::string &value);

    /*!
     * \brief Closes the container display list.
     */
    void end_draw_containers() override;

    /*!
     * \brief Creates and opens the display list for stater draws.
     */
    void start_draw_states() override;

    /*!
     * \brief Draw a state of the trace.
     * \param start the beginning time of the state.
     * \param end the ending time of the state.
     * \param base vertical position of the state.
     * \param height the state height.
     * \param r the red color rate of the state.
     * \param g the green color rate of the state.
     * \param b the blue color rate of the state.
     */
    void draw_state(const Element_pos, const Element_pos, const Element_pos, const Element_pos, const Element_pos, EntityValue *) override;

    /*!
     * \brief Closes the state display list.
     */
    void end_draw_states() override;

    /*!
     * \brief Open the arrow display list.
     */
    void start_draw_arrows() override;

    /*!
     * \brief Draw an arrow.
     * \param start_time the beginning time of the arrow.
     * \param end_time the ending time of the arrow.
     * \param start_height vertical position of the begining time of the arrow.
     * \param end_height vertical position of the ending time of the arrow.
     *
     * This function stores all the information of the arrow to display it each time the render area need to be updated.
     */
    void draw_arrow(const Element_pos start_time, const Element_pos end_time, const Element_pos start_height, const Element_pos end_height, const Element_col r, const Element_col g, const Element_col b, EntityValue *value) override;

    /*!
     * \brief Closes the arrow display list.
     */
    void end_draw_arrows() override;

    /*!
     * \brief Draw arrows contained in the Arrow_ vector
     * \param arrows An arrow vector.
     */
    // void draw_stored_arrows(std::vector<Arrow_> &arrows);

    void start_draw_events() override;

    /*!
     * \brief Draw an event.
     * \param time time when the event occurs.
     * \param height vertical position of the event.
     * \param container_height information to draw event. It corresponds to the container height when they are drawn horizontally.
     *
     *
     * \brief Creates and opens the display list for container draws.
     *
     * This function stores all the information of the event to display it each time the render area need to be updated.
     */
    void draw_event(const Element_pos time, const Element_pos height, const Element_pos container_height, EntityValue *value) override;

    void end_draw_events() override;
    /*!
     * \brief Draw events contained in the Event_ vector
     * \param events An event vector.
     */
    //  void draw_stored_events(std::vector<Event_> &events);

    /*!
     * \brief Creates and opens the display list for counter draws.
     */
    void start_draw_counter() override;

    /*!
     * \brief Draw a point of the counter.
     * \param x x position of the point.
     * \param y y position of the point.
     *
     * Each time counter is increased, this function is called with the coordinates of the new point.
     */
    void draw_counter(const Element_pos x, const Element_pos y);

    /*!
     * \brief Closes the counter display list.
     */
    void end_draw_counter() override;

    /*!
     * \brief Called before ruler drawing.
     */
    void start_ruler() override;

    /*!
     * \brief Called after ruler drawing.
     */
    void end_ruler() override;

    /*!
     * \brief Do nothing (it is present for compatibility of the Render class).
     */
    void end_draw() override;

    /*!
     * \brief Set the color for the further drawings.
     * \param r the red value. Within [0 ; 1].
     * \param g the green value. Within [0 ; 1].
     * \param b the blue value. Within [0 ; 1].
     */
    void set_color(float r, float g, float b) override;

    /*!
     * \brief Draw a text.
     * \param x the horizontal position of the left bottom corner of the text.
     * \param y the vertical position of the left bottom corner of the text.
     * \param z the deep position of the text.
     * \param s the text.
     */
    void draw_text(const Element_pos x, const Element_pos y, const Element_pos z, const std::string s) override;

    /*!
     * \brief Draw a quad.
     * \param x the horizontal position of the left bottom corner of the quad.
     * \param y the vertical position of the left bottom corner of the quad.
     * \param z the deep position of the quad.
     * \param w the width of the quad.
     * \param h the height of the quad.
     */
    void draw_quad(Element_pos x, Element_pos y, Element_pos z, Element_pos w, Element_pos h) override;

    /*!
     * \brief Draw a triangle.
     * \param x the horizontal position of the triangle center.
     * \param y the vertical position of the triangle center.
     * \param size the edge size.
     * \param r the rotation of triangle. (clockwise and in degree)
     */
    void draw_triangle(Element_pos x, Element_pos y,
                       Element_pos size, Element_pos r) override;

    /*!
     * \brief Draw a line.
     * \param x1 the horizontal position of the first point.
     * \param y1 the vertical position of the firt point.
     * \param x2 the horizontal position of the second point.
     * \param y2 the vertical position of the second point.
     * \param z the deep position of the triangle.
     */
    void draw_line(Element_pos x1, Element_pos y1, Element_pos x2, Element_pos y2, Element_pos z) override;

    /*!
     * \brief Draw a circle.
     * \param x the horizontal position of the circle center.
     * \param y the vertical position of the circle center.
     * \param z the deep position of the circle.
     * \param r the circle radius.
     */
    void draw_circle(Element_pos x, Element_pos y, Element_pos z, Element_pos r) override;

    /*!
     * \brief Draw a text with the value of a variable
     * \param text text to draw.
     * \param y y position of the point.
     */
    void draw_text_value(long int id, double text, double y) override {
        (void)id;
        (void)text;
        (void)y;
    }

    /*!
     * \brief draws the vertical helper line
     */

    void draw_vertical_line() override;

    /*!
     * \brief slot connected to the simple click event
     */
    void update_vertical_line() override;

    /*!
     * \brief set the vertical line offset
     * \param l the line offset.
     */
    void set_vertical_line(Element_pos l) override;

    /*!
     * \brief Draw the ruler display list.
     */
    void call_ruler() override;
};

#endif
