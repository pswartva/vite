/**
 *
 * @file src/parser/PajeParser/PajeFileManager.hpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Johnny Jazeix
 * @author Mathieu Faverge
 * @author Francois Trahay
 * @author Thibault Soucarre
 * @author Olivier Lagrasse
 * @author Augustin Degomme
 * @author Clement Vuchener
 *
 * @date 2024-07-17
 */

/**
 * \file    PajeFileManager.hpp
 *  Visual Trace Explorer
 *
 *  Release Date: January, 2nd 2010
 *  ViTE is a software to vivisualize execution trace provided by
 *  a group of student from ENSEIRB and INRIA Bordeaux - Sud-Ouest
 *
 * @version 1.1
 * @author  Kevin Coulomb
 * @author  Mathieu Faverge
 * @author  Johnny Jazeix
 * @author  Olivier Lagrasse
 * @author  Jule Marcoueille
 * @author  Pascal Noisette
 * @author  Arthur Redondy
 * @author  Clément Vuchener
 * @date    2010-01-02
 *
 **/

#ifndef FILE_HPP
#define FILE_HPP

#include <string.h>

#define _PAJE_NBMAXTKS 32

/**
 * \struct PajeLine
 *
 * Brief structure to store information read
 * on each line
 *
 * \param _id Line number
 * \param _nbtks Number of tokens found on the line
 * \param _tokens Pointers on the found tokens
 *
 */
typedef struct PajeLine
{
    int _id;
    int _nbtks;
    char **_tokens;

    PajeLine() = default;
} PajeLine_t;

/**
 *  \class PajeFileManager
 *
 *  File manager to read files using Paje syntax.
 *  Each line is read one after one and stored in
 *  the PajeLine structure associated to the class.
 *
 * \sa Parser
 * \sa ParserPaje
 * \sa ParserVite
 */
class PajeFileManager : public std::ifstream
{

private:
    std::string _filename;
    long long _filesize { -1 };

    unsigned int _lineid { 0 };
    int _nbtks { 0 };
    char **_tokens;
    std::string _line;

    PajeFileManager(const PajeFileManager &);

public:
    /*!
     *  \brief Constructor for the file
     */
    PajeFileManager();

    /*!
     *  \brief  Constructor for the file
     *  \param  filename : a filename
     */
    PajeFileManager(const char *filename, ios_base::openmode mode = ios_base::in);

    /*!
     *  \brief Destructor
     *  Destroy the file
     */
    ~PajeFileManager() override;

    /*!
     *  \fn open(const char * filename, ios_base::openmode mode)
     *  \brief Open the file
     *  \param filename the file name
     *  \param mode the opening mode
     */
    void open(const char *filename, ios_base::openmode mode = ios_base::in);

    /*!
     *  \fn close()
     *  \brief Close the file if already opened
     */
    void close();

    /*!
     *  \fn get_filesize() const;
     *  \return The length of the file opened
     */
    long long get_filesize() const;

    /*!
     *  \fn get_size_loaded();
     *  \return The size already loaded
     */
    long long get_size_loaded();

    /*!
     *  \fn get_percent_loaded()
     *  \return The percent of the file loaded (between 0 and 1).
     */
    float get_percent_loaded();

    /*!
     *  \fn get_line(PajeLine *lineptr)
     *  \brief Get the next line.
     *  \param lineptr the line filled in
     *  \return the next line
     */
    int get_line(PajeLine *lineptr);

    /*!
     *  \fn print_line()
     *  \brief Print the current line for debug
     */
    void print_line();
};

#endif // FILE_HPP
