/**
 *
 * @file src/statistics/Statistic.hpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Johnny Jazeix
 * @author Mathieu Faverge
 * @author Kevin Coulomb
 * @author Olivier Lagrasse
 *
 * @date 2024-07-17
 */
#ifndef STATISTIC_HPP
#define STATISTIC_HPP

/*!
 * \file Statistic.hpp
 */
class Container;
class EntityValue;
class Statistic;

/*!
 * \struct stats
 * \brief Structure used to keep information used for statistics.
 * It keeps the states for an EntityValue.
 */
struct stats
{
    double _total_length;
    int _cardinal;
    // Ajout
    stats(double length, int cardinal = 1) :
        _total_length(length), _cardinal(cardinal) { }
};

/*!
 * \class Statistic
 * \brief Class used to keep information used for statistics
 * It keeps the states for a Container.
 */
class Statistic
{
private:
    std::map<const EntityValue *, stats *> _states;
    std::map<const Container *, int> _link;
    int _number_link;
    int _event;

public:
    /*!
     * \fn Statistic()
     * \brief default constructor
     */
    Statistic();

    /*!
     * \fn ~Statistic()
     * \brief Destructor
     */
    ~Statistic();

    /*!
     * \fn add_state(EntityValue const * val, double length)
     * \brief Add a new entry to the state map
     */
    void add_state(EntityValue const *val, double length);

    /*!
     * \fn add_link(const Container * target)
     * \brief Add a new entry to the link map
     */
    void add_link(const Container *target);

    /*!
     * \fn set_nb_event(int number);
     * \brief set the number of event
     */
    void set_nb_event(int number);

    /*!
     * \fn get_nb_event() const;
     * \brief get the number of event
     */
    int get_nb_event() const;

    /*!
     * \fn get_states();
     * \brief get the states
     */
    std::map<const EntityValue *, stats *> get_states();
};

#endif
