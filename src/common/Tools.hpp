/**
 *
 * @file src/common/Tools.hpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Johnny Jazeix
 * @author Mathieu Faverge
 *
 * @date 2024-07-17
 */
/*!
 *\file Tools.hpp
 *\brief This file contains functions that are used in different modules and do not belong to a particular class.
 */

#ifndef TOOLS_HPP
#define TOOLS_HPP

/*!
 * \fn convert_to_double(const std::string &arg)
 * \brief Convert the string in double
 * \param arg the string to be doublized.
 * \param val the double well converted.
 * \return true if the conversion has succeded, false else
 */
bool convert_to_double(const std::string &arg, double *val);

// #define MIN(x,y) (((x)<(y))?(x):(y))
// #define MAX(x,y) (((x)<(y))?(y):(x))

/*!
  \fn clockGet(void)
  \brief Timing routine.
  Uses different timing routines depending on the machine architecture.
  \return Returns the time ellapsed since 'clockStart'.
 */
double clockGet();

#endif // TOOLS_HPP
