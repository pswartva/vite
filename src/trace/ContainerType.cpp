/**
 *
 * @file src/trace/ContainerType.cpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Johnny Jazeix
 * @author Mathieu Faverge
 * @author Augustin Degomme
 * @author Clement Vuchener
 *
 * @date 2024-07-17
 */
#include <string>
#include <list>
/* -- */
#include "trace/values/Values.hpp"
#include "trace/ContainerType.hpp"
/* -- */
using namespace std;

ContainerType::ContainerType(Name &name, ContainerType *parent) :
    _name(name), _parent(parent), _children() {
}

ContainerType::~ContainerType() {
    // Delete children
    while (!_children.empty()) {
        delete _children.front();
        _children.pop_front();
    }
}

void ContainerType::add_child(ContainerType *child) {
    _children.push_back(child);
}

const std::string ContainerType::get_name() const {
    return _name.get_name();
}

const Name ContainerType::get_Name() const {
    return _name;
}

const ContainerType *ContainerType::get_parent() const {
    return _parent;
}

const list<ContainerType *> *ContainerType::get_children() const {
    return &_children;
}
