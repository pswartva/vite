/**
 *
 * @file src/trace/tree/Interval.hpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Kevin Coulomb
 * @author Camille Ordronneau
 * @author Johnny Jazeix
 * @author Clement Vuchener
 *
 * @date 2024-07-17
 */
/*!
 *
 *\file Interval.hpp
 * \brief File that contains the definition of an interval
 *
 */

#ifndef INTERVAL_HPP
#define INTERVAL_HPP

/*!
 * \class Interval
 * \brief A simple interval (time, distance ...) with left and right values.
 */

class Interval
{
public:
    /*!
     * \var _left
     * \brief left value of the interval
     */
    Date _left;
    /*!
     * \var  _right
     * \brief right value of the interval
     */
    Date _right;

    /*!
     * \fn Interval()
     * \brief Default constructor
     */
    Interval();
    /*!
     * \fn Interval(Date left, Date right)
     * \brief constructor
     * \param left left value of the interval
     * \param right right value of the interval
     */
    Interval(Date left, Date right);
};

#endif
