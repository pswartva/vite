/**
 *
 * @file src/trace/Entity.cpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Johnny Jazeix
 * @author Mathieu Faverge
 * @author Thibault Soucarre
 * @author Augustin Degomme
 * @author Luigi Cannarozzo
 * @author Clement Vuchener
 *
 * @date 2024-07-17
 */

#include <string>
#include <map>
#include <list>
#include <vector>
#include <stack>
/* -- */
#include "trace/values/Values.hpp"
#include "trace/EntityTypes.hpp"
#include "trace/Entitys.hpp"
#include "trace/Entity.hpp"
/* -- */
using namespace std;

Entity::Entity(Container *container, map<std::string, Value *> opt) :
    _container(container) /*, _extra_fields(opt)*/ {
    if (opt.empty()) {
        _extra_fields = nullptr;
    }
    else {
        _extra_fields = new map<std::string, Value *>(opt);
        opt.clear();
    }
}

Entity::Entity() = default;

const Container *Entity::get_container() const {
    return _container;
}

const map<std::string, Value *> *Entity::get_extra_fields() const {
    return _extra_fields;
}

Entity::~Entity() {
    this->clear();
}

void Entity::clear() {
    if (_extra_fields != nullptr) {
        delete _extra_fields;
    }
    _extra_fields = nullptr;
}
