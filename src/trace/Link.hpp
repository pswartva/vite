/**
 *
 * @file src/trace/Link.hpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Johnny Jazeix
 * @author Mathieu Faverge
 * @author Thibault Soucarre
 * @author Kevin Coulomb
 * @author Clement Vuchener
 *
 * @date 2024-07-17
 */
#ifndef LINK_HPP
#define LINK_HPP

/*!
 * \file Link.hpp
 */

class Container;
class LinkType;

/*!
 * \class Link
 * \brief Describe a link between two containers
 */
class Link : public Entity
{
public:
    typedef std::list<Link *> Vector;
    typedef std::list<Link *>::const_iterator VectorIt;

private:
    Date _start, _end;
    LinkType *_type;
    EntityValue *_value;
    Container *_source, *_destination;

public:
    /*!
     * \brief Constructor
     */
    Link(Date start, Date end, LinkType *type, Container *container, Container *source, Container *destination, EntityValue *value, std::map<std::string, Value *> opt);

    /*!
     * \fn get_start_time() const;
     * \brief Get the start time of the link
     */
    Date get_start_time() const;

    /*!
     * \fn get_end_time() const
     * \brief Get the end time of the link
     */
    Date get_end_time() const;

    /*!
     * \fn get_duration() const
     * \brief Get the duration of the link
     */
    double get_duration() const;

    /*!
     * \fn get_type() const
     * \brief Get the type of the link
     */
    const LinkType *get_type() const;

    /*!
     * \fn get_value() const
     * \brief Get the value of the link
     * \return Pointer to the Entityvalue or NULL if it has no value
     */
    EntityValue *get_value() const;

    /*!
     * \fn get_source() const
     * \brief Get the source container of the link
     */
    const Container *get_source() const;

    /*!
     * \fn get_destination() const
     * \brief Get the destination container of the link
     */
    const Container *get_destination() const;
};

#endif
