/**
 *
 * @file src/trace/values/Date.cpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Johnny Jazeix
 * @author Olivier Lagrasse
 * @author Mathieu Faverge
 *
 * @date 2024-07-17
 */

#include <string>
#include <sstream>
#include <iomanip> // For std::setprecision
/* -- */
#include "common/Tools.hpp"
#include "common/common.hpp"
#include "common/Info.hpp"
/* -- */
#include "trace/values/Value.hpp"
#include "trace/values/Date.hpp"

#include <iostream>

Date::Date() {
    _is_correct = true;
}

Date::Date(double value) :
    _value(value) {
    //     std::cout << "min: " << Info::Entity::x_min << " max: " << Info::Entity::x_max << " current: " << value << std::endl;
    _is_correct = true;
    if (_value > Info::Entity::x_max)
        Info::Entity::x_max = _value;
    else if (Info::Entity::x_min > _value)
        Info::Entity::x_min = _value;
}

Date::Date(const std::string &value) {
    _is_correct = convert_to_double(value, &_value);

    if (_value > Info::Entity::x_max)
        Info::Entity::x_max = _value;
    else if ((Info::Entity::x_min > _value) && (_value != -1.))
        Info::Entity::x_min = _value;
}

std::string Date::to_string() const {
    std::ostringstream oss;
    oss << std::setprecision(Value::_PRECISION) << _value;
    return oss.str();
}

double Date::get_value() const {
    return _value;
}

bool Date::operator<(const Date &d) const {
    return (_value < d._value);
}

bool Date::operator>(const Date &d) const {
    return (_value > d._value);
}

bool Date::operator<=(const Date &d) const {
    return (_value <= d._value);
}

bool Date::operator>=(const Date &d) const {
    return (_value >= d._value);
}

double Date::operator-(const Date &d) const {
    return (_value - d._value);
}
