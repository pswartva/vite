/**
 *
 * @file src/trace/values/Name.hpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Johnny Jazeix
 * @author Mathieu Faverge
 * @author Thibault Soucarre
 * @author Kevin Coulomb
 * @author Pascal Noisette
 * @author Olivier Lagrasse
 * @author Augustin Degomme
 *
 * @date 2024-07-17
 */
#ifndef NAME_HPP
#define NAME_HPP
/*!
 *
 * \file Name.hpp
 *
 */
/*!
 *
 * \class Name
 * \brief Store a name and/or an alias for trace objects
 *
 */
class String;

class Name : public Value
{
private:
    std::string _alias, _name;

public:
    /*!
     * \brief Constructor
     */
    Name();
    Name(std::string name, std::string alias);
    Name(const std::string &name);
    Name(const String &name);

    /*!
     * \fn set_name(std::string name)
     * \brief Set the name
     * \param name Name of the object
     *
     */
    void set_name(std::string name);

    /*!
     * \fn set_alias(std::string alias)
     * \brief Set the alias
     * \param alias Alias of the object
     *
     */
    void set_alias(std::string alias);

    /*!
     *
     * \fn to_string() const
     * \return The name of the object, if it exists, the alias otherwise
     *
     */
    std::string to_string() const override;

    /*!
     *
     * \fn to_string() const
     * \return The name of the object
     *
     */
    std::string get_name() const;

    /*!
     *
     * \fn to_string() const
     * \return The alias
     *
     */
    std::string get_alias() const;

    /*!
     *
     * \fn to_String() const
     * \return The name of the object, if it exists, the alias otherwise
     *
     */
    String to_String() const;

    /*!
     *
     * \fn operator== (String &) const
     * \brief Compare the string with the name or the alias
     * \return true if the string equals the name or the alias
     *
     */
    bool operator==(String &) const;
    bool operator==(Name &) const;

    /*!
     *
     * \fn operator== (std::string &) const
     * \brief Compare the string with the name or the alias
     * Provided for convenience
     * \return true if the string equals the name or the alias
     *
     */
    bool operator==(std::string &) const;

    bool operator<(std::string &s) const;
    bool operator<(const Name &s) const;

    friend std::ostream &operator<<(std::ostream &os, const Name &obj);
};

#endif // NAME_HPP
