/**
 *
 * @file src/trace/EventType.hpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Johnny Jazeix
 * @author Mathieu Faverge
 * @author Clement Vuchener
 *
 * @date 2024-07-17
 */
#ifndef EVENTTYPE_HPP
#define EVENTTYPE_HPP

/*!
 * \file EventType.hpp
 */
/*!
 * \class EventType
 * \brief Describe the event types
 */
class EventType : public EntityType
{
private:
public:
    /*!
     * \fn EventType(const Name &name, ContainerType *container_type, std::map<std::string, Value *> opt)
     * \brief EventTytpe Constructor
     * \param name Name of the type
     * \param container_type Type of the container for event of this type
     * \param opt optional data about the event
     */
    EventType(const Name &name, ContainerType *container_type, std::map<std::string, Value *> opt);
    // EventType();
};

#endif
